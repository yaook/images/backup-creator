##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.13.1-bookworm
RUN apt update && \
    apt install tini mariadb-backup mariadb-client etcd-client -y

ARG build_packages="apt-utils git make gcc libssl-dev libcap-ng-dev python3 unbound autoconf automake libtool  libunbound-dev"
ARG ovs_version=3.1.1
RUN apt-get update && \
    apt-get install -y $build_packages libaio1 libatomic1 libunbound8 && \
    git clone -b v${ovs_version} https://github.com/openvswitch/ovs && \
    cd ovs && \
    ./boot.sh && \
    ./configure --localstatedir=/var --sysconfdir=/etc --prefix=/usr  && \
    make && \
    make install && \
    rm -r /ovs &&\
    apt-get purge -y $build_packages && apt-get autoremove --purge -y  && \
    apt-get clean && rm -rf /var/lib/apt/lists

ADD backup_creator /app/backup_creator
COPY setup.py /app/

RUN pip3 install -e /app

WORKDIR /app

ENTRYPOINT /usr/bin/tini -s -- python3 -m backup_creator

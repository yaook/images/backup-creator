#
# Copyright (c) 2022 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from setuptools import setup

setup(
    name="backup-creator",
    version="0.0.1",
    packages=["backup_creator"],
    install_requires=[
        "environ-config==24.1.0",
        "cron-converter==1.2.1",
        "hvac==2.3.0",
        "ovs==3.4.1",
        "ovsdbapp==2.10.0"
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'backup-now=backup_creator.__main__:run_once'
        ]
    },
)

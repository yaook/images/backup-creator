#!/usr/bin/env python
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import functools
import typing
import attr as environ_attr
from datetime import datetime, timezone
import environ
from enum import Enum
import importlib
import logging.config
import time

import cron_converter
from pathlib import Path
import shutil


_DATETIME_FORMAT = "%Y-%m-%d_%H:%M:%S.%f"

_PROGRAM_NAME = "backup_creator"

# Default logfile
DEFAULT_LOGFILE = None

# Default loglevel (as string)
DEFAULT_LOGLEVEL = "INFO"

_YAOOK_BACKUP_PREFIX = "YAOOK_BACKUP_CREATOR"


logger = logging.getLogger(_PROGRAM_NAME)


class EnvVarShortName(Enum):
    BACKUP_MODULE_NAME = f'{_YAOOK_BACKUP_PREFIX}_BACKUP_MODULE_NAME'
    OUT_PATH = f'{_YAOOK_BACKUP_PREFIX}_OUT_PATH'
    TMP_PATH = f'{_YAOOK_BACKUP_PREFIX}_TMP_PATH'
    SCHEDULE = f'{_YAOOK_BACKUP_PREFIX}_SCHEDULE'
    LOGFILE = f'{_YAOOK_BACKUP_PREFIX}_LOGFILE'
    LOGLEVEL = f'{_YAOOK_BACKUP_PREFIX}_LOGLEVEL'


def _validate_pod_name(instance, attribute, value):
    if not value:
        return
    if "-" not in value:
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} does "
            "not seem to be a pod in a statefulset. it is missing a '-'. "
            f"Was: '{value}'")
    parts = value.split("-")
    try:
        int(parts[-1])
    except ValueError:
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} does "
            "not seem to be a pod in a statefulset. The index is non numeric."
            f" Was: '{value}'")


def _validate_parent_dir_exists(instance, attribute, value):
    if value is None:
        return
    if not (value.is_file() or not value.exists()):
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} "
            f"must either be non-existent or a file. Was: '{value}'.")
    if not value.parent.is_dir():
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} must "
            f"point to a (non-existing or existing) file in "
            f"an existing directory. Was: '{value}'.")


def _validate_log_level(instance, attribute, value):
    try:
        logging._checkLevel(value)
    except ValueError:
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value}: "
            f"{value} is not a valid loglevel."
        )


def _validate_cron_expression(instance, attribute, value):
    try:
        cron_converter.Cron(value)
    except ValueError:
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value}: "
            f"{value} is not a valid cron expression."
        )


def _validate_module_name(instance, attribute, value):
    try:
        module = importlib.import_module(value)
    except ModuleNotFoundError:
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value}: "
            f"Module {value} is not a valid python module."
        )
    else:
        try:
            module.Config
            module.create_backup
        except AttributeError:
            raise ValueError(
                f"{getattr(EnvVarShortName, attribute.name.upper()).value}: "
                f"Module {value} does not have required member."
            )


@environ.config(prefix=_YAOOK_BACKUP_PREFIX)
class CreatorConfig:
    pod_name = environ.var(
        validator=_validate_pod_name,
        default=None,
        help="The name of the pod we are running in. "
             "This is only available when running in a statefulset. "
             "When set it ensures that only on the pod with index 0 backups "
             "are run."
    )

    backup_module_name = environ.var(
        validator=_validate_module_name,
        help="The python module that does the actually backup. "
             "Must contain a Config class and a create_backup function"
    )

    out_path = environ.var(
        converter=Path,
        help="The output path for the finalized backup. "
             "Must be an existing directory. "
             "If using the backup-shifter, must point to the `new` directory.",
    )

    tmp_path = environ.var(
        converter=environ_attr.converters.optional(Path),
        default=None,
        help="The temporary path for creating the backup. "
             "If set must be a directory on the same filesystem as "
             f"{EnvVarShortName.OUT_PATH}."
             "If not set will be the directory `incoming` which is "
             "placed in the same parent directory as "
             f"{EnvVarShortName.OUT_PATH}.",
    )

    schedule = environ.var(
        validator=_validate_cron_expression,
        help="The cron expression specifying when to run a backup.",
    )

    logfile = environ.var(
        validator=_validate_parent_dir_exists,
        converter=environ_attr.converters.optional(Path),
        default=DEFAULT_LOGFILE,
        help=f"Path to the log file to use. "
             f"Default value: {DEFAULT_LOGFILE}. ",
    )

    loglevel = environ.var(
        validator=_validate_log_level,
        default=DEFAULT_LOGLEVEL,
        help=f"Log level to use. Default value: {DEFAULT_LOGLEVEL}. ",
    )

    scale_size = environ.var(
        converter=int,
        default=int(1),
        help="Replica size of the database statefulset.",
    )


class BackupCreator(object):
    program_name = _PROGRAM_NAME

    description = ("Backup creator is configurable by the following "
                   "environment variables: "
                   f"{', '.join([e.value for e in EnvVarShortName])}. "
                   "It will process the specified service every time the "
                   "cron expression matches. If the creation of the backup "
                   "finishes after the next run should start then that run "
                   "will be skipped. "
                   "Environment variables: "
                   f"{CreatorConfig.generate_help()}")  # type: ignore

    _creator_config = None

    def __init__(self):
        if self._creator_config is None:
            # initialize lazily to allow printing the help message without
            # setting any of the required env vars.
            try:
                self._creator_config = environ.to_config(CreatorConfig)
            except Exception as e:
                msg = f"Exception: {e}. " \
                      f"Environment variables: {CreatorConfig.generate_help()}"
                raise EnvironmentError(msg)

        self._setup_logger(
            log_level=self._creator_config.loglevel,
            log_file=self._creator_config.logfile,
        )

        self.stateful_index = self._get_stateful_index(
            self._creator_config.pod_name)

        self.scale_size = self._creator_config.scale_size - 1

        self.backup_function = self._get_backup_instance(
            self._creator_config.backup_module_name)

        self.out_path = self._creator_config.out_path
        self.tmp_path = self._creator_config.tmp_path
        self._setup_dirs()

        self.schedule_string = self._creator_config.schedule
        self.schedule_cron = cron_converter.Cron(self.schedule_string)

        reference = datetime.now()
        self.schedule = self.schedule_cron.schedule(reference)

    @staticmethod
    def _setup_logger(log_level, log_file=None):
        formatters = {
            "standard": {
                "format": '%(asctime)s - %(name)s - '
                          '%(levelname)s - %(message)s'
            }
        }
        handlers = {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "standard",
                "level": log_level,
                "stream": "ext://sys.stdout",
            },
        }
        if log_file:
            handlers["file"] = {
                "class": "logging.handlers.RotatingFileHandler",
                "formatter": "standard",
                "filename": log_file,
                "maxBytes": 1024,
                "backupCount": 3,
            }
        loggers = {
            "backup_creator": {
                "handlers": list(handlers.keys()),
                "level": log_level,
            }
        }
        config = {
            "version": 1,
            "handlers": handlers,
            "formatters": formatters,
            "loggers": loggers,
        }
        logging.config.dictConfig(config)

    def _get_stateful_index(self, pod_name: typing.Optional[str]
                            ) -> typing.Optional[int]:
        if not pod_name:
            return None
        parts = pod_name.split("-")
        return int(parts[-1])

    def _get_backup_instance(
            self,
            name: str,
    ) -> (typing.Callable[[Path], None]):
        module = importlib.import_module(name)
        config_class = getattr(module, "Config")
        function = getattr(module, "create_backup")
        cfg = environ.to_config(config_class)
        return functools.partial(function, cfg)

    def _setup_dirs(self) -> None:
        logger.info("Initializing directories")

        self.out_path.mkdir(parents=True, exist_ok=True)

        if self.tmp_path is None:
            self.tmp_path = self.out_path.parent / "incoming"
            logger.info("Using %s as temporary path", self.tmp_path)

        if not self.tmp_path.exists():
            self.tmp_path.mkdir(parents=True)

    def do_backup(self):
        """Run the actual backup.

        Can be called from the outside to trigger one single backup.
        """

        try:
            logger.info("Running backup job")
            backup_path = self._get_backup_path()
            logger.info("Backup path is %s", backup_path)

            self.backup_function(backup_path)

            if not backup_path.exists():
                raise ValueError(
                    "Backup did not create an output file or directory")

            logger.info("Backup finished")

            target_path = self.out_path / backup_path.name
            backup_path.replace(target_path)

            logger.info("Backup moved to output")
        except Exception:
            logger.exception(
                "Failed to run backup. Retrying next iteration.")
            if backup_path and backup_path.exists():
                if backup_path.is_dir():
                    shutil.rmtree(backup_path)
                else:
                    backup_path.unlink()

    def start(self, loop_forever=True):
        """Runs the backup job for each time the cron expression matches.

        Finalized backups are placed in self.out_dir. It is assumed that
        removing these backups from that location is done by some other
        service.

        If `self.stateful_index` is set and the current pod is not the last
        in the deployment then the method will just wait forever.

        :param loop_forever: flag indicating whether the outer loop should loop
            forever or only be run once. Used for testing.
        :type loop_forever: bool
        """

        if self.stateful_index is not None and \
           self.stateful_index < self.scale_size:
            self._sleep_forever()
            return

        while True:
            next_schedule = self._get_next_schedule()
            self.wait_until(next_schedule)

            self.do_backup()

            if not loop_forever:
                return

    def _sleep_forever(self) -> None:
        logger.info(
            "We should not run on this node. Sleeping forever"
        )
        while True:
            time.sleep(86400)

    def wait_until(self, target: datetime) -> None:
        logger.info(
            "Waiting for next execution at "
            f"{target.strftime(_DATETIME_FORMAT)}."
        )
        secs = (target - datetime.now()).total_seconds()
        if secs > 0:
            time.sleep(secs)

    def _get_next_schedule(self) -> datetime:
        next_schedule = self.schedule.next()
        if next_schedule < datetime.now():
            logger.warning(
                "Next scheduled execution is at "
                f"{next_schedule.strftime(_DATETIME_FORMAT)} "
                "but now is "
                f"{self._now_as_str()}. Skipping execution until we "
                "catch up again.")
            while next_schedule < datetime.now():
                next_schedule = self.schedule.next()
        return next_schedule

    def _get_backup_path(self) -> Path:
        """Returns the path where a newly created backup should be stored"""
        backup_path = self.tmp_path / self._now_as_str()
        if backup_path.exists():
            raise ValueError(
                f"Backup path {backup_path} already exists. "
                "This should not happen!")
        return backup_path

    @staticmethod
    def _now_as_str() -> str:
        """Returns the current time in utc time zone as string """
        return datetime.now(timezone.utc).strftime(_DATETIME_FORMAT)

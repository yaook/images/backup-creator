#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import subprocess
import environ
import socket
from pathlib import Path

from ovs.stream import Stream
from ovsdbapp.backend.ovs_idl import connection, idlutils
from ovsdbapp.schema.open_vswitch import impl_idl as idl_ovs
import backup_creator.backup_creator


_YAOOK_BACKUP_OVSDB_PREFIX = backup_creator.backup_creator.\
    _YAOOK_BACKUP_PREFIX + "_OVSDB"


logger = logging.getLogger(__name__)


@environ.config(prefix=_YAOOK_BACKUP_OVSDB_PREFIX)
class Config:
    database = environ.var(
        help="The database to be backed up."
    )

    socket = environ.var(
        help="The socket to use to connect to the northbound or"
             "southbound daemon. If several sockets are set,"
             "the leader will be selected."
    )

    certificate = environ.var(
        help="",
        default="/etc/ssl/private/tls.crt"
    )

    private_key = environ.var(
        help="",
        default="/etc/ssl/private/tls.key"
    )

    ca_certificate = environ.var(
        help="",
        default="/etc/ssl/private/ca.crt"
    )


def find_follower(config: Config) -> str:
    """
    A backup should not be performed on the leader node due to interruptions
    during the database snapshot.
    Therefore we try to find a follower node to perform a safe backup.
    """

    if len(config.socket.split(",")) > 1:
        logger.info(
            "Ovsdb is running in clustered mode, searching for a follower node"
            " in order to not interrupt the leader."
        )
        conn = config.socket.split(",")
        p = conn[0].split(":")
        # when ovsdb is compiled without unbound,
        # we cannot use DNS names when specifying OVSDB remotes.
        ip = socket.gethostbyname(p[1])
        first_target = f"{p[0]}:{ip}:{p[2]}"
        Stream.ssl_set_private_key_file(config.private_key)
        Stream.ssl_set_certificate_file(config.certificate)
        Stream.ssl_set_ca_cert_file(config.ca_certificate)
        i = connection.OvsdbIdl.from_server(first_target, "_Server")
        c = connection.Connection(idl=i, timeout=10)
        row = (
            idl_ovs.OvsdbIdl(c)
            .db_find_rows("Database", ("name", "=", config.database))
            .execute()
            .pop()
        )
        if not idlutils.get_column_value(row=row, col="leader"):
            logger.info(
                f"Backup will be done from first ovsdb node: {conn[0]}"
            )
            return conn[0]
        else:
            logger.info(
                f"Backup will be done from second ovsdb node: {conn[1]}"
            )
            return conn[1]
    else:
        logger.info("Ovsdb is running in standalone mode")
        return config.socket


def create_backup(config: Config, backup_path: Path) -> None:
    try:
        logger.info("Creating output directories.")
        backup_path.mkdir()
        ovsdb_path = backup_path / "ovsdb"
        ovsdb_path.mkdir()

        logger.info("Running ovsdb-client backup")

        output_file = ovsdb_path / config.database
        args = ["ovsdb-client", "--private-key=" + config.private_key,
                "--certificate=" + config.certificate,
                "--ca-cert=" + config.ca_certificate,
                "--no-leader-only",  # do no interrupt the leader
                "backup", find_follower(config),
                config.database, '>', str(output_file)]

        subprocess.check_call([
            " ".join(args)
            ],
            shell=True
            )

        logger.info("Finished")

    except subprocess.CalledProcessError as e:
        raise e

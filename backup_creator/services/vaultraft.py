#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import hvac
import logging
import pathlib

import environ

import backup_creator.backup_creator


_YAOOK_BACKUP_VAULT_RAFT_PREFIX = backup_creator.backup_creator.\
    _YAOOK_BACKUP_PREFIX + "_VAULT_RAFT"


logger = logging.getLogger(__name__)


@environ.config(prefix=_YAOOK_BACKUP_VAULT_RAFT_PREFIX)
class Config:
    approle_path = environ.var(
        help="Path to the approle authentication engine"
    )

    approle_role_id = environ.var(
        help="Role ID of the approle user to use"
    )

    approle_secret_id = environ.var(
        help="Secret ID of the approle user to use"
    )


def create_backup(config: Config, backup_path: pathlib.Path) -> None:
    client = hvac.Client()
    if client.is_authenticated():
        revoke_token = False
    else:
        logger.debug("authenticating to vault")
        client.auth.approle.login(
            role_id=config.approle_role_id,
            secret_id=config.approle_secret_id,
            mount_point=config.approle_path,
        )
        if not client.is_authenticated():
            raise RuntimeError(
                "failed to authenticate to vault in a strange way",
            )
        revoke_token = True

    try:
        snapshot_response = client.sys.take_raft_snapshot()
        snapshot_response.raise_for_status()
        with backup_path.open("wb") as f:
            for chunk in snapshot_response.iter_content(chunk_size=8192):
                f.write(chunk)
    finally:
        client.logout(revoke_token=revoke_token)

#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import subprocess
import time

import environ
from pathlib import Path
import shutil

import backup_creator.backup_creator


_YAOOK_BACKUP_MARIADB_PREFIX = backup_creator.backup_creator.\
    _YAOOK_BACKUP_PREFIX + "_MARIADB"

_YAOOK_BACKUP_PREFIX = backup_creator.backup_creator.\
    _YAOOK_BACKUP_PREFIX

logger = logging.getLogger(__name__)


@environ.config(prefix=_YAOOK_BACKUP_MARIADB_PREFIX)
class Config:
    username = environ.var(
        help="The username to use to connect to mariadb."
    )

    password = environ.var(
        help="The password to use to connect to mariadb."
    )

    database = environ.var(
        help="The database to be backed up."
    )

    mysqldump = environ.bool_var(
        default=False,
        help="Execute a mysqldump for each table in addition to "
             "the normal backup. NOTE: This dump is done without a global "
             "read lock and may therefor be inconsistent."
    )

    socket = environ.var(
        default="/var/run/mysqld/mysqld.sock",
        help="The socket to use to connect to the mariadb daemon. "
             "If unset the following /var/run/mysqld/mysqld.sock is used."
    )

    retry_count = environ.var(
        default=5,
        help="Number of times the mariabackup --prepare should be retried."
    )

    retry_wait = environ.var(
        default=300,
        help="Seconds between a retry of a failed mariabackup prepare."
    )


@environ.config(prefix=_YAOOK_BACKUP_PREFIX)
class BackupCreatorConfig:
    scale_size = environ.var(
        converter=int,
        default=int(1),
        help="Replica size of the database statefulset.",
    )


def set_db_desync(desired_state: str, config: Config) -> bool:
    backup_creator_config = environ.to_config(BackupCreatorConfig)
    if backup_creator_config.scale_size == 1:
        logger.info("No need to set desync state for single DB node.")
        return True

    if desired_state == "enable":
        logger.info("Disabling sync with other DB node(s)")
        state = "ON"
    else:
        logger.info("Re-Enabling sync with other DB node(s)")
        state = "OFF"

    try:
        subprocess.check_call([
            "mysql",
            "-e",
            f"SET GLOBAL wsrep_desync = {state}",
            f"--user={config.username}",
            f"--password={config.password}",
            f"--socket={config.socket}",
        ])
    except subprocess.CalledProcessError as e:
        logger.error(f"Could set state `{desired_state}` for desync. {e}")
        return False

    return True


def create_mariabackup(config: Config, backup_path: Path) -> None:
    mariabackup_path = backup_path / "mariabackup"
    logger.info(f"Creating output directory {mariabackup_path}.")
    if not mariabackup_path.exists():
        mariabackup_path.mkdir()

    logger.info("Running mariabackup")
    subprocess.check_call([
        "/mariabackup/mariadb-backup",
        "--backup",
        f"--target-dir={mariabackup_path.absolute()}",
        f"--databases={config.database}",
        f"--user={config.username}",
        f"--password={config.password}",
        f"--socket={config.socket}",
    ])

    logger.info("Finished mariabackup")


def create_mysqldump(config: Config, backup_path: Path) -> None:
    backup_creator_config = environ.to_config(BackupCreatorConfig)

    if config.mysqldump:
        logger.info("Running mysqldump")
        mysqldump_path = backup_path / "mysqldump"
        mysqldump_path.mkdir()

        tables = subprocess.check_output([
            "mysql",
            config.database,
            "-e",
            "show tables;",
            "--batch",
            "--silent",
            f"--user={config.username}",
            f"--password={config.password}",
            f"--socket={config.socket}",
        ], encoding="utf-8").splitlines()

        logger.info("Running mysqldump for following tables: %s",
                    tables)
        for table in tables:
            output_file = mysqldump_path / table
            if backup_creator_config.scale_size == 1:  # single DB node
                mysqldump = subprocess.check_output([
                    "mysqldump",
                    config.database,
                    table,
                    f"--user={config.username}",
                    f"--password={config.password}",
                    f"--socket={config.socket}",
                ])
            else:
                mysqldump = subprocess.check_output([
                    "mysqldump",
                    config.database,
                    table,
                    f"--user={config.username}",
                    f"--password={config.password}",
                    f"--socket={config.socket}",
                    "--skip-lock-tables",
                ])
            with open(output_file, "wb") as f:
                f.write(mysqldump)

        logger.info("Finished mysqldumps")


def prepare_mariabackup_for_restore(config: Config, backup_path: Path) -> None:
    logger.info("Preparing backup for possible restore")
    mariabackup_path = backup_path / "mariabackup"
    subprocess.check_call([
        "/mariabackup/mariadb-backup",
        "--prepare",
        f"--target-dir={mariabackup_path.absolute()}",
    ])
    logger.info("Finished preparing backup for possible restore")


def do_complete_mariabackup(config: Config, backup_path: Path) -> None:
    logger.info("Proceeding with the Mariabackup")
    for i in range(config.retry_count):
        try:
            if set_db_desync("enable", config):
                create_mariabackup(config, backup_path)
                prepare_mariabackup_for_restore(config, backup_path)
        except subprocess.CalledProcessError as e:
            set_db_desync("disable", config)
            if i >= config.retry_count:
                raise e
            logger.warn(
                "The mariabackup failed. "
                f"Retrying in {config.retry_wait/60} minutes."
            )
            time.sleep(config.retry_wait)
            mariabackup_path = backup_path / "mariabackup"
            shutil.rmtree(mariabackup_path)
        else:
            break


def create_backup(config: Config, backup_path: Path) -> None:
    try:
        backup_path.mkdir()

        if set_db_desync("enable", config):
            create_mysqldump(config, backup_path)
            do_complete_mariabackup(config, backup_path)
            set_db_desync("disable", config)
        else:
            logger.error("Could not set desync state for DB nodes. Aborting"
                         "backup creation")

    except subprocess.CalledProcessError as e:
        logger.error("Could not create mysqldump and/or mariabackup backup.")
        e.cmd = [x.replace(config.password, 'XXX') for x in e.cmd]
        raise e
    finally:
        set_db_desync("disable", config)

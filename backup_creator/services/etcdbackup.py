#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import environ
from pathlib import Path
import subprocess

import backup_creator.backup_creator


_YAOOK_BACKUP_ETCD_PREFIX = backup_creator.backup_creator.\
    _YAOOK_BACKUP_PREFIX + "_ETCD"

logger = logging.getLogger(__name__)


@environ.config(prefix=_YAOOK_BACKUP_ETCD_PREFIX)
class Config:
    port = environ.var(
        default="2379",
        help="The port etcd cluster is listening to"
    )

    host = environ.var(
        default="127.0.0.1",
        help="The ip address of the etcd-cluster the backup is targeting"
    )

    cacert = environ.var(
        default="/etc/kubernetes/pki/etcd/ca.crt",
        help="Path to the cacert file on the kubernetes master"
             "(keep in mind where the directory is mounted)"
    )

    certfile = environ.var(
        default="/etc/kubernetes/pki/etcd/server.crt",
        help="Path to the server cert file on the kubernetes master"
             "(keep in mind where the directory is mounted)"
    )

    certkey = environ.var(
        default="/etc/kubernetes/pki/etcd/server.key",
        help="Path to the server cert key file on the kubernetes master"
             "(keep in mind where the directory is mounted)"
    )


def create_backup(config: Config, backup_path: Path) -> None:
    try:
        logger.info("Creating output directories")
        backup_path.mkdir()

        logger.info("Running etcd snapshot")

        filename = backup_path / "etcdsnapshot"
        if config.cacert:
            url = "https://{}:{}".format(config.host, config.port)
            args = [
                "--cacert", config.cacert,
                "--cert", config.certfile,
                "--key", config.certkey,
            ]
        else:
            url = "http://{}:{}".format(config.host, config.port)
            args = []

        try:
            subprocess.check_output(
                [
                    "etcdctl",
                    "--endpoints", url,
                    *args,
                    "snapshot", "save", filename.as_posix()],
                env={"ETCDCTL_API": "3"},
            )
        except subprocess.CalledProcessError as e:
            logger.error("Failed to run etcdctl: %s", e)
            logger.error("STDOUT: %s", e.output)
            raise

        logger.info("Finished etcd snapshot")
    except Exception as e:
        logging.exception(e)
        raise e

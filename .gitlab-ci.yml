##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
---
stages:
  - test
  - build
  - manifest
  - bumpversion

lint_python:
  stage: test
  tags:
    - docker
  image:
    name: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.13"
  script:
    - pip install flake8
    - flake8
  needs: []

python_typecheck:
  stage: test
  tags:
    - docker
  image:
    name: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.13"
  script:
    - pip3 install -e .
    - pip3 install mypy
    - touch backup_creator/__init__.py
    - mypy --config-file mypy.ini -p backup_creator
  needs: []

unittest:
  stage: test
  tags:
    - docker
  image:
    name: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.13"
  script:
    # Really install our package now
    - pip install -e .
    - pip install -r requirements-test.txt
    - pip install nose2 coverage
    - python -m nose2 tests
  needs: []
  artifacts:
    when: always
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

build:
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:27.5.1"
  parallel:
    matrix:
      - TARGETARCH: ["amd64", "arm64"]
  variables:
    DOCKER_TLS_CERTDIR: ""
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_BUILDKIT: 1
  services:
    - name: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:27.5.1-dind"
      alias: docker
  before_script:
    - mkdir $HOME/.docker
    - docker login "$REGISTRY_URL" -u "$REGISTRY_USER" -p "$REGISTRY_PASS"
    - docker login -u "$CI_DEPENDENCY_PROXY_USER" -p "$CI_DEPENDENCY_PROXY_PASSWORD" "$CI_DEPENDENCY_PROXY_SERVER"
    # The following is magic to get the dockerhub images using the gitlab dependency proxy without specifying these images in the Dockerfile
    # This allows use to ignore the dockerhub rate limit while allowing security analyzers to still pick up dockerhub changes
    - sed -i "s|^FROM |FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/|" Dockerfile
  stage: build
  script:
    - BRANCH_IMAGE_NAME="$REGISTRY_URL/$REGISTRY_PROJECT/$CI_PROJECT_NAME"
    - if [ "${TARGETARCH}" = "amd64" ]; then TARGETARCH_SUFFIX=""; else TARGETARCH_SUFFIX="-${TARGETARCH}"; fi
    - BRANCH_IMAGE_TAG="${BRANCH_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}-${TARGETARCH}"
    - docker build . -f Dockerfile -t "$BRANCH_IMAGE_TAG" --platform "linux/${TARGETARCH}"
    - docker push "$BRANCH_IMAGE_TAG"
    - if [ "$CI_COMMIT_REF_PROTECTED" = "true" ]; then VERSION=$(cat .next-version); RELEASE_IMAGE_TAG="${BRANCH_IMAGE_NAME}:${VERSION}-${TARGETARCH}"; docker tag "$BRANCH_IMAGE_TAG" "$RELEASE_IMAGE_TAG"; docker push "$RELEASE_IMAGE_TAG"; fi
  except:
    - tags
  tags:
    - docker-${TARGETARCH}

manifest:
  stage: manifest
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:27.5.1"
  variables:
    DOCKER_TLS_CERTDIR: ""
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_BUILDKIT: 1
  services:
    - name: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:27.5.1-dind"
      alias: docker
  before_script:
    - mkdir ${HOME}/.docker
    - docker login "${REGISTRY_URL}" -u "${REGISTRY_USER}" -p "${REGISTRY_PASS}"
  script:
    - BRANCH_IMAGE_NAME="${REGISTRY_URL}/${REGISTRY_PROJECT}/${CI_PROJECT_NAME}"
    - BRANCH_IMAGE_TAG="${BRANCH_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    - docker manifest create "${BRANCH_IMAGE_TAG}" "${BRANCH_IMAGE_TAG}-amd64" "${BRANCH_IMAGE_TAG}-arm64"
    - docker manifest push "${BRANCH_IMAGE_TAG}"
    - |
      if [ "${CI_COMMIT_REF_PROTECTED}" = "true" ]; then
        VERSION=$(cat .next-version)
        RELEASE_IMAGE_TAG="${BRANCH_IMAGE_NAME}:${VERSION}"
        docker manifest create "${RELEASE_IMAGE_TAG}" "${RELEASE_IMAGE_TAG}-amd64" "${RELEASE_IMAGE_TAG}-arm64"
        docker manifest push "${RELEASE_IMAGE_TAG}"
      fi
  except:
    - tags

version:
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:latest"
  stage: bumpversion
  script:
    - pip install bump2version
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - bump2version --current-version "$(cat .next-version)" patch .next-version --commit
    - git remote set-url origin "https://gitlab-ci-token:${BOT_GIT_PUSH_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    - git push -o ci.skip origin HEAD:${CI_COMMIT_REF_NAME}
  only:
    - devel

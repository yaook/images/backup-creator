<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# backup-creator

Simple docker container to create backups for various services and move them
to a directory for https://gitlab.com/yaook/images/backup-shifter to process.

## Configuration

The backup-creator is configured using environment variables.

- `BACKUP_CREATOR_BACKUP_CLASS_NAME`: The module to use for backups. Corresponds to the
  the service that should be backed up.
- `BACKUP_CREATOR_OUT_PATH`: Must point to the `new` directory of the
  `backup-shifter`
- `BACKUP_CREATOR_SCHEDULE`: A cron-style schedule to define when backups should run

## Extension

To add your own scripts for creating backups, mount a valid python module
to /app/backup-creator/services/. You can then use it by setting the
`BACKUP_CREATOR_MODULE` environment variable.

## Manual trigger

When having a backup-creator container in place and configured somewhere you can
shell into the container and run `backup-now` to trigger a backup irrespective
of the normal backup shedule.

## License

[Apache 2](LICENSE.txt)

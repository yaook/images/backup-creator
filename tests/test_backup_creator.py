#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from datetime import datetime
import os
import tempfile
import unittest
import unittest.mock
from unittest.mock import sentinel

import cron_converter
from freezegun import freeze_time
from pathlib import Path

from backup_creator.backup_creator import BackupCreator


class TestBackupCreator(unittest.TestCase):
    """
    Tests the class BackupCreator.
    """
    @freeze_time("1970-01-01 00:00:01")
    def setUp(self):
        super().setUp()
        self.t_dir = tempfile.TemporaryDirectory()
        self.out_dir = Path(self.t_dir.name) / "new"
        self.tmp_dir = Path(self.t_dir.name) / "incoming"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_POD_NAME": "test123-0",
            "YAOOK_BACKUP_CREATOR_BACKUP_MODULE_NAME":
                "tests.fake_backup",
            "YAOOK_BACKUP_CREATOR_OUT_PATH":
                self.out_dir.as_posix(),
            "YAOOK_BACKUP_CREATOR_TMP_PATH":
                self.tmp_dir.as_posix(),
            "YAOOK_BACKUP_CREATOR_SCHEDULE": "0 */4 * * *",
        }):

            self.creator = BackupCreator()

        """ self.work_dir = tempfile.TemporaryDirectory()
        self.shifters_dir = tempfile.TemporaryDirectory()

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_backup_creator_WORK_DIR": self.work_dir.name,
            "YAOOK_backup_creator_SHIFTERS_DIR": self.shifters_dir.name,
        }):

            self.shifter = BackupCreator()

        self.new_dir = Path(self.work_dir.name, "new")
        self.warm_dir = Path(self.work_dir.name, "warm")
        self.scratch_dir = Path(self.work_dir.name, "scratch")

        self.new_file_event = \
            (None, [MASK_LOOKUP[IN_MOVED_TO]], self.new_dir, "new_dummy_file")
        self.warm_file_event = \
            (None, [MASK_LOOKUP[IN_MOVED_TO]], self.warm_dir,
             "warm_dummy_file")
        self.new_dir_event = (
            None,
            [MASK_LOOKUP[IN_MOVED_TO], MASK_LOOKUP[IN_ISDIR]],
            self.new_dir,
            "new_test_dir",
        ) """

    def tearDown(self):
        self.t_dir.__enter__()
        self.t_dir.__exit__(None, None, None)

    @unittest.mock.patch("tests.fake_backup.create_backup")
    def test___init__(self, create_backup):
        expected_out_path = Path(self.out_dir.name)
        expected_tmp_path = expected_out_path.parent / "test"
        expected_schedule_cron = cron_converter.Cron("0 */4 * * *")

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_POD_NAME": "test123-123",
            "YAOOK_BACKUP_CREATOR_BACKUP_MODULE_NAME":
                "tests.fake_backup",
            "YAOOK_BACKUP_CREATOR_OUT_PATH":
                expected_out_path.as_posix(),
            "YAOOK_BACKUP_CREATOR_TMP_PATH":
                expected_tmp_path.as_posix(),
            "YAOOK_BACKUP_CREATOR_SCHEDULE": "0 */4 * * *",
            "YAOOK_BACKUP_CREATOR_SCALE_SIZE": "1"
        }):
            creator = BackupCreator()

        self.assertEqual(creator.stateful_index, 123)
        self.assertEqual(creator.out_path, expected_out_path)
        self.assertEqual(creator.tmp_path, expected_tmp_path)
        self.assertEqual(creator.schedule_cron, expected_schedule_cron)

        creator.backup_function(sentinel.path)
        create_backup.assert_called_once_with(
            unittest.mock.ANY,
            sentinel.path,
        )

    def test___init___defaults(self):
        expected_out_path = Path(self.out_dir.name)
        expected_tmp_path = expected_out_path.parent / "incoming"
        expected_schedule_cron = cron_converter.Cron("0 */4 * * *")

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_BACKUP_MODULE_NAME":
                "tests.fake_backup",
            "YAOOK_BACKUP_CREATOR_OUT_PATH":
                expected_out_path.as_posix(),
            "YAOOK_BACKUP_CREATOR_SCHEDULE": "0 */4 * * *",
        }):
            creator = BackupCreator()

        self.assertEqual(creator.stateful_index, None)
        self.assertEqual(creator.out_path, expected_out_path)
        self.assertEqual(creator.tmp_path, expected_tmp_path)
        self.assertEqual(creator.schedule_cron, expected_schedule_cron)

    def test___init___raises_wrong_class_name(self):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_BACKUP_MODULE_NAME": "abc123",
        }):
            with self.assertRaisesRegex(
                    EnvironmentError,
                    "YAOOK_BACKUP_CREATOR_BACKUP_MODULE_NAME"):
                BackupCreator()

    def test___init___raises_wrong_cron_schedule(self):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_SCHEDULE": "1",
        }):
            with self.assertRaisesRegex(
                    EnvironmentError,
                    "YAOOK_BACKUP_CREATOR_SCHEDULE"):
                BackupCreator()

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator.wait_until")
    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator._get_next_schedule")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_waits_until_schedule(self, _get_next_schedule, wait_until):
        _get_next_schedule.return_value = \
            unittest.mock.sentinel.next_schedule

        self.creator.start(loop_forever=False)

        wait_until.assert_called_once_with(
            unittest.mock.sentinel.next_schedule)

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator.wait_until")
    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator._get_backup_path")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_runs_backup(self, _get_backup_path, wait_until):
        _get_backup_path.return_value = \
            unittest.mock.sentinel.backup_path
        _get_backup_path.return_value.exists = unittest.mock.Mock()
        _get_backup_path.return_value.replace = unittest.mock.Mock()

        with unittest.mock.patch.object(
                self.creator,
                "backup_function") as backup:
            self.creator.start(loop_forever=False)

        backup.assert_called_once_with(
            unittest.mock.sentinel.backup_path)

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator.wait_until")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_moves_finished_backup(self, wait_until):
        self.assertEqual(0, len(list(self.tmp_dir.iterdir())))
        self.assertEqual(0, len(list(self.out_dir.iterdir())))

        self.creator.start(loop_forever=False)

        self.assertEqual(0, len(list(self.tmp_dir.iterdir())))
        self.assertEqual(1, len(list(self.out_dir.iterdir())))

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator.wait_until")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_if_no_backup_created(self, wait_until):
        with unittest.mock.patch.object(
                self.creator,
                "backup_function"):
            self.creator.start(loop_forever=False)

        self.assertCountEqual([], list(self.out_dir.iterdir()))

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator.wait_until")
    @unittest.mock.patch(
        "pathlib.Path.replace")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_cleanup_on_exception(self, replace, wait_until):
        replace.side_effect = ValueError

        self.creator.start(loop_forever=False)

        self.assertEqual(0, len(list(self.tmp_dir.iterdir())))
        self.assertEqual(0, len(list(self.out_dir.iterdir())))

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator.wait_until")
    @unittest.mock.patch(
        "tests.fake_backup.create_backup")
    @unittest.mock.patch(
        "pathlib.Path.replace")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_cleanup_on_exception_directory(self, replace, backup,
                                                  wait_until):
        replace.side_effect = ValueError

        def backup(self, path):
            path.mkdir()
        backup.side_effect = backup

        self.creator.start(loop_forever=False)

        self.assertEqual(0, len(list(self.tmp_dir.iterdir())))
        self.assertEqual(0, len(list(self.out_dir.iterdir())))

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator._sleep_forever")
    @unittest.mock.patch(
        "tests.fake_backup.create_backup")
    @freeze_time("1970-01-01 00:00:00")
    def test_start_waits_forever_non_primary(self, backup, _sleep_forever):
        self.creator.stateful_index = 1
        self.creator.scale_size = 2

        self.creator.start(loop_forever=False)

        _sleep_forever.assert_called_once()
        backup.assert_not_called()

    @unittest.mock.patch("time.sleep")
    @freeze_time("1970-01-01 00:00:00")
    def test_wait_until(self, sleep):
        end = datetime.fromtimestamp(3600)

        self.creator.wait_until(end)

        sleep.assert_called_with(3600)

    @unittest.mock.patch("time.sleep")
    @freeze_time("1970-01-02 00:00:00")
    def test_wait_until_does_nothing_if_later(self, sleep):
        end = datetime.fromtimestamp(3600)

        self.creator.wait_until(end)

        sleep.assert_not_called()

    @freeze_time("1970-01-01 00:00:01")
    def test__get_next_schedule(self):
        next = self.creator._get_next_schedule()

        self.assertEqual(next, datetime.fromtimestamp(14400))  # 04:00

    @freeze_time("1970-01-01 12:00:01")
    def test__get_next_schedule_skips_if_in_future(self):
        next = self.creator._get_next_schedule()

        self.assertEqual(next, datetime.fromtimestamp(57600))  # 16:00

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator._now_as_str")
    def test__get_backup_path(self, _now_as_str):
        _now_as_str.return_value = "abc_123"

        ret = self.creator._get_backup_path()

        self.assertEqual(ret, self.tmp_dir / "abc_123")

    @unittest.mock.patch(
        "backup_creator.backup_creator.BackupCreator._now_as_str")
    def test__get_backup_path_raises_if_exists(self, _now_as_str):
        _now_as_str.return_value = "tmp"
        self.creator.tmp_path = Path("/")

        self.assertRaises(ValueError, self.creator._get_backup_path)

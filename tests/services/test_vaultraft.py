#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import environ
import os
import tempfile
import unittest
import unittest.mock
from unittest.mock import sentinel, patch

from pathlib import Path

import backup_creator.services.vaultraft as vaultraft


class Testcreate_backup(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        self._stack = contextlib.ExitStack()
        self.t_dir = self._stack.enter_context(tempfile.TemporaryDirectory())
        self.backup_path = Path(self.t_dir) / "backup"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_VAULT_RAFT_APPROLE_PATH":
                str(sentinel.approle_path),
            "YAOOK_BACKUP_CREATOR_VAULT_RAFT_APPROLE_ROLE_ID":
                str(sentinel.approle_role_id),
            "YAOOK_BACKUP_CREATOR_VAULT_RAFT_APPROLE_SECRET_ID":
                str(sentinel.approle_secret_id),
        }):
            self.config = environ.to_config(vaultraft.Config)

    def tearDown(self) -> None:
        self._stack.close()
        super().tearDown()

    @patch("hvac.Client")
    def test_backup_creates_backup_using_approle_login(self, Client):
        def generate_data():
            yield b"foo"
            yield b"bar"
            yield b"baz"

        def is_authenticated():
            yield False  # to force approle
            yield True  # to confirm that it worked

        client = unittest.mock.Mock()
        response = unittest.mock.Mock()
        Client.return_value = client

        client.is_authenticated.side_effect = is_authenticated()
        client.sys.take_raft_snapshot.return_value = response

        response.raise_for_status.return_value = None
        response.iter_content.return_value = generate_data()

        vaultraft.create_backup(self.config, self.backup_path)

        with self.backup_path.open("rb") as f:
            self.assertEqual(f.read(), b"foobarbaz")

        Client.assert_called_once_with()
        client.auth.approle.login.assert_called_once_with(
            role_id=str(sentinel.approle_role_id),
            secret_id=str(sentinel.approle_secret_id),
            mount_point=str(sentinel.approle_path),
        )
        client.sys.take_raft_snapshot.assert_called_once_with()
        response.raise_for_status.assert_called_once_with()
        client.logout.assert_called_once_with(revoke_token=True)

    @patch("hvac.Client")
    def test_backup_creates_backup_using_external_auth(self, Client):
        def generate_data():
            yield b"foo"
            yield b"bar"
            yield b"baz"

        def is_authenticated():
            yield True  # to indicate external auth

        client = unittest.mock.Mock()
        response = unittest.mock.Mock()
        Client.return_value = client

        client.is_authenticated.side_effect = is_authenticated()
        client.sys.take_raft_snapshot.return_value = response

        response.raise_for_status.return_value = None
        response.iter_content.return_value = generate_data()

        vaultraft.create_backup(self.config, self.backup_path)

        with self.backup_path.open("rb") as f:
            self.assertEqual(f.read(), b"foobarbaz")

        Client.assert_called_once_with()
        client.is_authenticated.assert_called_once_with()
        client.auth.approle.login.assert_not_called()
        client.sys.take_raft_snapshot.assert_called_once_with()
        response.raise_for_status.assert_called_once_with()
        client.logout.assert_called_once_with(revoke_token=False)

    @patch("hvac.Client")
    def test_create_backup_logs_out_even_on_error(self, Client):
        class FooException(Exception):
            pass

        def is_authenticated():
            yield True  # to indicate external auth

        client = unittest.mock.Mock()
        response = unittest.mock.Mock(["raise_for_status"])
        Client.return_value = client

        client.is_authenticated.side_effect = is_authenticated()
        client.sys.take_raft_snapshot.return_value = response

        response.raise_for_status.side_effect = FooException

        with self.assertRaises(FooException):
            vaultraft.create_backup(self.config, self.backup_path)

        self.assertFalse(self.backup_path.exists())

        Client.assert_called_once_with()
        client.is_authenticated.assert_called_once_with()
        client.auth.approle.login.assert_not_called()
        client.sys.take_raft_snapshot.assert_called_once_with()
        response.raise_for_status.assert_called_once_with()
        client.logout.assert_called_once_with(revoke_token=False)

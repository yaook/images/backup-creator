#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import os
import tempfile
import unittest
import unittest.mock
from unittest.mock import sentinel, patch

from pathlib import Path

import backup_creator.services.mariadbservice as mariadbservice


class TestConfig(unittest.TestCase):
    def test_defaults(self):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_MARIADB_USERNAME":
                str(sentinel.username),
            "YAOOK_BACKUP_CREATOR_MARIADB_PASSWORD":
                str(sentinel.password),
            "YAOOK_BACKUP_CREATOR_MARIADB_DATABASE":
                str(sentinel.database),
        }):
            config = environ.to_config(mariadbservice.Config)

        self.assertFalse(config.mysqldump)
        self.assertEqual(config.socket, "/var/run/mysqld/mysqld.sock")


class TestMariaDBService(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.t_dir = tempfile.TemporaryDirectory()
        self.backup_path = Path(self.t_dir.name) / "backup"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_MARIADB_USERNAME":
                str(sentinel.username),
            "YAOOK_BACKUP_CREATOR_MARIADB_PASSWORD":
                str(sentinel.password),
            "YAOOK_BACKUP_CREATOR_MARIADB_DATABASE":
                str(sentinel.database),
            "YAOOK_BACKUP_CREATOR_MARIADB_SOCKET":
                str(sentinel.socket),
        }):
            self.config = environ.to_config(mariadbservice.Config)

    @patch("subprocess.check_call")
    def test_backup_no_dump(self, _run_command):
        mariabackup = self.backup_path / "mariabackup"

        mariadbservice.create_backup(self.config, self.backup_path)

        self.assertTrue(self.backup_path.exists())
        self.assertTrue(mariabackup.exists())

        _run_command.assert_any_call([
            "/mariabackup/mariadb-backup",
            "--backup",
            f"--target-dir={mariabackup.absolute()}",
            f"--databases={str(sentinel.database)}",
            f"--user={str(sentinel.username)}",
            f"--password={str(sentinel.password)}",
            f"--socket={str(sentinel.socket)}",
        ])

        _run_command.assert_any_call([
            "/mariabackup/mariadb-backup",
            "--prepare",
            f"--target-dir={mariabackup.absolute()}",
        ])

    @patch("subprocess.check_call")
    @patch("subprocess.check_output")
    def test_backup_with_dump_clustered(self, _get_output, _run_command):
        testtables = ["table1", "table2", "table3"]
        mysqldump = self.backup_path / "mysqldump"
        self.config.mysqldump = True
        _get_output.side_effect = [
            '\n'.join(testtables),
        ] + [
            table.encode("utf-8")
            for table in testtables
        ]

        mariadbservice.get_mariadbversion = unittest.mock.Mock([])
        mariadbservice.get_mariadbversion.return_value = "10.X"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_SCALE_SIZE":
                str(1),
        }):
            mariadbservice.create_backup(self.config, self.backup_path)

        self.assertEqual(
            _get_output.call_args_list[0],
            unittest.mock.call([
                "mysql",
                str(sentinel.database),
                "-e",
                "show tables;",
                "--batch",
                "--silent",
                f"--user={str(sentinel.username)}",
                f"--password={str(sentinel.password)}",
                f"--socket={str(sentinel.socket)}",
            ], encoding="utf-8"))

        self.assertEqual(3, len(list(mysqldump.iterdir())))

        for table in testtables:
            _get_output.assert_any_call([
                "mysqldump",
                str(sentinel.database),
                table,
                f"--user={str(sentinel.username)}",
                f"--password={str(sentinel.password)}",
                f"--socket={str(sentinel.socket)}",
            ])
            with open(mysqldump / table) as f:
                content = f.read()
                self.assertEqual(table, content)

    @patch("subprocess.check_call")
    @patch("subprocess.check_output")
    def test_backup_with_dump_single_node(self, _get_output, _run_command):
        testtables = ["table1", "table2", "table3"]
        mysqldump = self.backup_path / "mysqldump"
        self.config.mysqldump = True
        _get_output.side_effect = [
            '\n'.join(testtables),
        ] + [
            table.encode("utf-8")
            for table in testtables
        ]

        mariadbservice.get_mariadbversion = unittest.mock.Mock([])
        mariadbservice.get_mariadbversion.return_value = "10.X"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_SCALE_SIZE":
                str(3),
        }):
            mariadbservice.create_backup(self.config, self.backup_path)

        self.assertEqual(
            _get_output.call_args_list[0],
            unittest.mock.call([
                "mysql",
                str(sentinel.database),
                "-e",
                "show tables;",
                "--batch",
                "--silent",
                f"--user={str(sentinel.username)}",
                f"--password={str(sentinel.password)}",
                f"--socket={str(sentinel.socket)}",
            ], encoding="utf-8"))

        self.assertEqual(3, len(list(mysqldump.iterdir())))

        for table in testtables:
            _get_output.assert_any_call([
                "mysqldump",
                str(sentinel.database),
                table,
                f"--user={str(sentinel.username)}",
                f"--password={str(sentinel.password)}",
                f"--socket={str(sentinel.socket)}",
                "--skip-lock-tables"
            ])
            with open(mysqldump / table) as f:
                content = f.read()
                self.assertEqual(table, content)

#
# Copyright (c) 2022 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import os
import tempfile
import unittest
import unittest.mock
from unittest.mock import sentinel, patch, MagicMock

from pathlib import Path

import backup_creator.services.ovsdbservice as ovsdbservice


class TestConfig(unittest.TestCase):
    def test_defaults(self):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_OVSDB_SOCKET":
                str(sentinel.socket),
            "YAOOK_BACKUP_CREATOR_OVSDB_DATABASE":
                str(sentinel.database),
        }):
            config = environ.to_config(ovsdbservice.Config)

        self.assertEqual(config.certificate, "/etc/ssl/private/tls.crt")
        self.assertEqual(config.private_key, "/etc/ssl/private/tls.key")
        self.assertEqual(config.ca_certificate, "/etc/ssl/private/ca.crt")


class TestOVSDBService(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.t_dir = tempfile.TemporaryDirectory()
        self.backup_path = Path(self.t_dir.name) / "backup"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_OVSDB_DATABASE":
                str(sentinel.database),
            "YAOOK_BACKUP_CREATOR_OVSDB_SOCKET":
                str(sentinel.socket),
        }):
            self.config = environ.to_config(ovsdbservice.Config)

    @patch("subprocess.check_call")
    def test_backup_with_dump(self, check_output):
        ovsdb_path = self.backup_path / "ovsdb"
        output_file = ovsdb_path / str(sentinel.database)

        ovsdbservice.create_backup(self.config, self.backup_path)
        self.assertTrue(self.backup_path.exists())

        check_output.assert_called_once_with(
            [
                "ovsdb-client " +
                "--private-key=/etc/ssl/private/tls.key " +
                "--certificate=/etc/ssl/private/tls.crt " +
                "--ca-cert=/etc/ssl/private/ca.crt " +
                "--no-leader-only " +
                "backup " +
                "sentinel.socket " +
                "sentinel.database " +
                "> " +
                str(output_file)
            ],
            shell=True
            )

    def test_find_follower_standalone(self):
        self.config.socket = "ssl:ovsdburl:6641"
        self.assertEqual(
            ovsdbservice.find_follower(self.config), self.config.socket
        )

    @patch("backup_creator.services.ovsdbservice.idlutils.get_column_value")
    @patch("backup_creator.services.ovsdbservice.idl_ovs.OvsdbIdl")
    @patch("backup_creator.services.ovsdbservice.connection.Connection")
    @patch(
        "backup_creator.services.ovsdbservice.connection.OvsdbIdl.from_server"
    )
    @patch("backup_creator.services.ovsdbservice.socket.gethostbyname")
    def test_find_follower_clustered_no_leader(
        self, hostip, ovsdbidl, ovsconn, idl_ovs, leader
    ):
        self.config.socket = (
            "ssl:ovsdburl1:6641,ssl:ovsdburl2:6641,ssl:ovsdburl3:6641"
        )
        hostip.return_value = "1.1.1.1"
        ovsdbidl.return_value = MagicMock()
        ovsconn.return_value = MagicMock()
        idl_ovs.return_value = MagicMock()

        self.assertEqual(
            ovsdbservice.find_follower(self.config),
            self.config.socket.split(",")[1],
        )

        leader.return_value = False
        self.assertEqual(
            ovsdbservice.find_follower(self.config),
            self.config.socket.split(",")[0],
        )

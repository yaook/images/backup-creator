#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import os
import tempfile
import unittest
from unittest.mock import sentinel, patch
from pathlib import Path

import backup_creator.services.etcdbackup as etcdbackup


class TestConfig(unittest.TestCase):
    def test_defaults(self):
        with patch.dict(os.environ, {}):
            config = environ.to_config(etcdbackup.Config)

        self.assertEqual(config.port, "2379")
        self.assertEqual(config.host, "127.0.0.1")
        self.assertEqual(config.cacert, "/etc/kubernetes/pki/etcd/ca.crt")
        self.assertEqual(config.certfile,
                         "/etc/kubernetes/pki/etcd/server.crt")
        self.assertEqual(config.certkey,
                         "/etc/kubernetes/pki/etcd/server.key")


class Testcreate_backup(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()

        self.t_dir = tempfile.TemporaryDirectory()
        self.backup_path = Path(self.t_dir.name) / "backup"

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_CREATOR_ETCD_PORT":
                str(sentinel.port),
            "YAOOK_BACKUP_CREATOR_ETCD_HOST":
                str(sentinel.host),
            "YAOOK_BACKUP_CREATOR_ETCD_CACERT":
                str(sentinel.cacert),
            "YAOOK_BACKUP_CREATOR_ETCD_CERTFILE":
                str(sentinel.certfile),
            "YAOOK_BACKUP_CREATOR_ETCD_CERTKEY":
                str(sentinel.certkey)
        }):
            self.config = environ.to_config(etcdbackup.Config)

    @patch("subprocess.check_output")
    def test_backup_does_backup(self, check_output):
        etcdSnapshot = self.backup_path / "etcdsnapshot"

        etcdbackup.create_backup(self.config, self.backup_path)

        self.assertTrue(self.backup_path.exists())

        check_output.assert_called_once_with(
            [
                'etcdctl',
                '--endpoints', 'https://sentinel.host:sentinel.port',
                '--cacert', 'sentinel.cacert',
                '--cert', 'sentinel.certfile',
                '--key', 'sentinel.certkey',
                'snapshot', 'save', etcdSnapshot.as_posix()
            ], env={'ETCDCTL_API': '3'}
        )

    @patch("subprocess.check_output")
    def test_backup_does_backup_http(self, check_output):
        self.config.cacert = None
        etcdSnapshot = self.backup_path / "etcdsnapshot"

        etcdbackup.create_backup(self.config, self.backup_path)

        self.assertTrue(self.backup_path.exists())

        check_output.assert_called_once_with(
            [
                'etcdctl',
                '--endpoints', 'http://sentinel.host:sentinel.port',
                'snapshot', 'save', etcdSnapshot.as_posix()
            ], env={'ETCDCTL_API': '3'}
        )

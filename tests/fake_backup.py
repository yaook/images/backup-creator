import environ
import pathlib


@environ.config
class Config:
    pass


def create_backup(config, backup_path: pathlib.Path) -> None:
    with backup_path.open("w") as _:
        pass
